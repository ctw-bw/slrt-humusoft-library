# Humusoft Library #

The Humusoft PCI cards for data acquisition and control (e.g. the Humusoft MF624) were made with support for Simulink Real-Time (formerly known as XPC).

![MF624 Photo](https://www.humusoft.com/data/img/datacq/mf624.jpg "MF624"){width=205 height=154}


However, since MATLAB 2018a the support for Simulink Real-Time has largely diminished, putting the focus on their own hardware: Speedgoat. Because of this there is no native support for the Humusoft cards in later versions of MATLAB.

However, the Humusoft Simulink blocks from MATLAB 2016b are just S-functions on top of the Humusoft C-library. So these blocks could be packed up and used also in later versions. That is exactly what this repository is.

## How to use ##

 1. Clone this repository
 2. Add the `humusoftlib/` directory to your MATLAB path
 3. Create your own Simulink model (*outside* this library directory, it can be anywhere on your computer)
 4. With your model and the library `slrt_humusoftlib_*.slx` model open, copy the necessary blocks from the library into your own model
     * The library model can be closed again afterwards

Test your model by trying to build it. The blocks you copied are masks. Because you added the library model to your path, MATLAB is able to find the necessary sources.

### Mex Files ###

Mex files compiled from C-code are committed to this repo too. Should you run into problems involving the mex files, you can recompile them by opening the directory of the .c files and running `mex [file.c]`.

## Versions ##

The library file is directly from MATLAB 2016b. For convenience, the file as re-saved with MATLAB 2018b, while running the upgrade advisor. Use this version for later versions.  
It could be worthwhile to create another upgrades version for later editions.

## Example ##

In the `examples/` directory you can find a demonstration of a Simulink Real-Time model for the MF624 card. This was saved in MATLAB 2018b.  
The example toggles the first digital output (DOUT0) and reads from the first digital input (DIN0). By connecting the two with a wire you should see the same signal twice.