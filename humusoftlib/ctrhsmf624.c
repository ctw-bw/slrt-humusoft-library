/* ctrhsmf624.c - xPC Target, non-inlined S-function driver for counter section of Humusoft MF624 board  */
/* Copyright 1996-2009 Humusoft s.r.o. and The MathWorks, Inc.
*/

#define 	S_FUNCTION_LEVEL 	2
#define 	S_FUNCTION_NAME 	ctrhsmf624

#include 	<stddef.h>
#include 	<stdlib.h>

#include 	"simstruc.h"

#ifdef 		MATLAB_MEX_FILE
#include 	"mex.h"
#else
#include 	<windows.h>
#include        "xpctarget.h"
#endif


/* Input Arguments */
#define NUM_PARAMS             (7)
#define SLOT_ARG               (ssGetSFcnParam(S,0))
#define DEV_ARG                (ssGetSFcnParam(S,1))
#define CHANNEL_ARG            (ssGetSFcnParam(S,2))
#define RESET_ARG              (ssGetSFcnParam(S,3))
#define CLOCK_ARG              (ssGetSFcnParam(S,4))
#define GATE_ARG               (ssGetSFcnParam(S,5))
#define SAMPLE_TIME_PARAM      (ssGetSFcnParam(S,6))

/* Convert S Function Parameters to Varibles */

#define SAMPLE_TIME            ((real_T) mxGetPr(SAMPLE_TIME_PARAM)[0])
#define SAMPLE_OFFSET          ((real_T) mxGetPr(SAMPLE_TIME_PARAM)[1])
#define SAMP_TIME_IND          (0)

/* IWork indexes */
enum
{
  BASE_ALTERA_I_IND,
  NO_I_WORKS    /* this must be the last item in enum */
};

#define NO_R_WORKS             (0)

static char_T msg[256];


/*====================*
 * S-function methods *
 *====================*/

static void mdlCheckParameters(SimStruct *S)
{
}

static void mdlInitializeSizes(SimStruct *S)
{
    size_t i;

    ssSetNumSFcnParams(S, NUM_PARAMS);
    if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)) {
        mdlCheckParameters(S);
        if (ssGetErrorStatus(S) != NULL) {
            return;
        }
    } else {
        return; /* Parameter mismatch will be reported by Simulink */
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);



    if (!ssSetNumInputPorts(S, 0)) return;

    if (!ssSetNumOutputPorts(S, (int_T) mxGetNumberOfElements(CHANNEL_ARG))) return;

    for (i=0;i<mxGetNumberOfElements(CHANNEL_ARG);i++) {
        ssSetOutputPortWidth(S, i, 1);
    }

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, 0);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for (i=0; i<NUM_PARAMS; i++)
      ssSetSFcnParamTunable(S, i, SS_PRM_NOT_TUNABLE);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE | SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME);
    ssSetSimStateCompliance(S, HAS_NO_SIM_STATE);
}



static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, SAMPLE_OFFSET);
}



#define MDL_START
static void mdlStart(SimStruct *S)
{

#ifndef MATLAB_MEX_FILE

    size_t i;
    uint_T channel;
    xpcPCIDevice pciinfo;
    void *Physical2;
    volatile uint32_T* base_altera;
    char devName[20];
    uint16_T devId;
    int32_T bus, slot, addr_position;


    switch ((int_T)mxGetPr(DEV_ARG)[0]) {
        case 1:
            strcpy(devName,"Humusoft MF624");
            devId=0x0624;
            addr_position = 4;
            break;
        case 3:
            strcpy(devName,"Humusoft MF634");
            devId=0x0634;
            addr_position = 3;
            break;
         }

    /* get bus and slot information */
    if (mxGetN(SLOT_ARG) == 1) {
        bus = 0;
        slot = (int32_T)mxGetPr(SLOT_ARG)[0];
    } else {
        bus = (int32_T)mxGetPr(SLOT_ARG)[0];
        slot = (int32_T)mxGetPr(SLOT_ARG)[1];
    }

    /* look for the PCI-Device */
    if (xpcGetPCIDeviceInfo(0x186C, devId, XPC_NO_SUB, XPC_NO_SUB, bus, slot, &pciinfo)) {
            sprintf(msg,"%s (bus %d,slot %d): board not present",devName, bus, slot );
            ssSetErrorStatus(S,msg);
            return;
    }

    /* Map I/O space and mark it read/write */
    Physical2 = (void *)pciinfo.BaseAddress[addr_position];
    base_altera = (volatile uint32_T *)xpcReserveMemoryRegion(Physical2, 128,
        XPC_RT_PG_USERREADWRITE);
    ssSetIWorkValue(S, BASE_ALTERA_I_IND, (int32_T) base_altera);

    for (i=0;i<mxGetNumberOfElements(CHANNEL_ARG);i++) {
        channel=(uint_T)mxGetPr(CHANNEL_ARG)[i]-1;

        base_altera[0x18] = 0x02 << (6*channel);        // stop counter
        base_altera[0x00+0x04*channel] = 0x03 | (((uint_T)mxGetPr(CLOCK_ARG)[0]+4) << 14);  // set counter mode
        base_altera[0x18] = 0x08 << (6*channel);        // reset counter
        base_altera[0x01+0x04*channel] = 0;             // reset Load A register
        base_altera[0x18] = 0x01 << (6*channel);        // start counter
    }

#endif

}

static void mdlOutputs(SimStruct *S, int_T tid)
{

#ifndef MATLAB_MEX_FILE

    size_t i;
    uint_T channel;
    volatile uint32_T* base_altera;
    real_T *y;

    base_altera = (volatile uint32_T*) ssGetIWorkValue(S, BASE_ALTERA_I_IND);

    for (i=0;i<mxGetNumberOfElements(CHANNEL_ARG);i++) {
        channel=(uint_T)mxGetPr(CHANNEL_ARG)[i]-1;
        y=ssGetOutputPortSignal(S,i);

        y[0]= (real_T) base_altera[0x01+0x04*channel];                // read counter
        if(mxGetPr(RESET_ARG)[0]==2)
          {
            base_altera[0x18] = 0x08 << (6*channel);        // reset counter
          }
    }

#endif

}

static void mdlTerminate(SimStruct *S)
{
}


#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif


