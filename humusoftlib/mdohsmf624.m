function [Sample, Channels] = mdohsmf624(flag, boardType, Channels, Sample)

% MDOHSMF624 - InitFcn and Mask Initialization for Humusoft MF624 digital output section

% Copyright 1996-2014 Humusoft s.r.o. and The MathWorks, Inc.


% Flag 0 - cross check for duplicate blocks
if flag == 0
  ck = mxpccrosscheckers;
  ck.pciuniq('Slot');
  return;
end


% Flag 1 - check parameters and compose mask display string

% Filter channels to eliminate duplicate - allow only one input port per
% channel
Channels = unique(Channels);

% display board type
brdtypes = {'MF624', 'AD622', 'MF634'};
maskDisplay = sprintf('disp(''%s\\nHumusoft\\nDigital Output'');\n', brdtypes{boardType});

% label output ports
maskDisplay = [maskDisplay ...
               sprintf('port_label(''input'', %d, ''%d'');\n', ...
                       [1:numel(Channels); Channels])];

% set MaskDisplay string
set_param(gcb, 'MaskDisplay', maskDisplay);

% check channels
MAX_CHANNEL = 8;
for i=1:numel(Channels)
  if (Channels(i)<1 || Channels(i)>MAX_CHANNEL)
    error(message('xPCTarget:MF624DO:ChannelValue', MAX_CHANNEL));
   end
end

% check sample time
switch numel(Sample)
  case 1
    Sample = [Sample 0];
  case 2
    % OK
  otherwise
    error(message('xPCTarget:MF624DO:SampleTime'));
end


%% EOF mdohsmf624.m
