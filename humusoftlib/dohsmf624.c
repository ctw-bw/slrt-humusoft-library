/* dohsmf624.c - xPC Target, non-inlined S-function driver for digital output section of HUMUSOFT MF624 board */
/* Copyright 1996-2009 Humusoft s.r.o. and The MathWorks, Inc.
*/

#define 	S_FUNCTION_LEVEL 	2
#define 	S_FUNCTION_NAME 	dohsmf624

#include 	<stddef.h>
#include 	<stdlib.h>

#include 	"simstruc.h"

#ifdef 		MATLAB_MEX_FILE
#include 	"mex.h"
#else
#include 	<windows.h>
#include        "xpctarget.h"
#endif


/* Input Arguments */
#define NUM_PARAMS             (4)
#define SLOT_ARG               (ssGetSFcnParam(S,0))
#define DEV_ARG                (ssGetSFcnParam(S,1))
#define CHANNEL_ARG            (ssGetSFcnParam(S,2))
#define SAMPLE_TIME_PARAM      (ssGetSFcnParam(S,3))

/* Convert S Function Parameters to Varibles */

#define SAMPLE_TIME            ((real_T) mxGetPr(SAMPLE_TIME_PARAM)[0])
#define SAMPLE_OFFSET          ((real_T) mxGetPr(SAMPLE_TIME_PARAM)[1])
#define SAMP_TIME_IND          (0)

/* IWork indexes */
enum
{
  BASE_ADC_I_IND,
  NO_I_WORKS    /* this must be the last item in enum */
};

#define NO_R_WORKS             (0)

static char_T msg[256];


/*====================*
 * S-function methods *
 *====================*/

static void mdlCheckParameters(SimStruct *S)
{
}

static void mdlInitializeSizes(SimStruct *S)
{
    size_t i;

    ssSetNumSFcnParams(S, NUM_PARAMS);
    if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)) {
        mdlCheckParameters(S);
        if (ssGetErrorStatus(S) != NULL) {
            return;
        }
    } else {
        return; /* Parameter mismatch will be reported by Simulink */
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, (int_T) mxGetNumberOfElements(CHANNEL_ARG))) return;

    for (i=0;i<mxGetNumberOfElements(CHANNEL_ARG);i++) {
        ssSetInputPortWidth(S, i, 1);
    }

    if (!ssSetNumOutputPorts(S, 0)) return;

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, 0);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for (i=0; i<NUM_PARAMS; i++)
      ssSetSFcnParamTunable(S, i, SS_PRM_NOT_TUNABLE);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE | SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME);
    ssSetSimStateCompliance(S, HAS_NO_SIM_STATE);
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, SAMPLE_OFFSET);

}


#define MDL_START
static void mdlStart(SimStruct *S)
{

#ifndef MATLAB_MEX_FILE
    xpcPCIDevice pciinfo;
    void *Physical1;
    volatile uint16_T* base_adc;
    char devName[20];
    uint16_T devId;
    int32_T bus, slot;


    switch ((int_T)mxGetPr(DEV_ARG)[0]) {
        case 1:
            strcpy(devName,"Humusoft MF624");
            devId=0x0624;
            break;
        case 2:
            strcpy(devName,"Humusoft AD622");
            devId=0x0622;
            break;
        case 3:
            strcpy(devName,"Humusoft MF634");
            devId=0x0634;
            break;
         }

    /* get bus and slot information */
    if (mxGetN(SLOT_ARG) == 1) {
        bus = 0;
        slot = (int32_T)mxGetPr(SLOT_ARG)[0];
    } else {
        bus = (int32_T)mxGetPr(SLOT_ARG)[0];
        slot = (int32_T)mxGetPr(SLOT_ARG)[1];
    }

    /* look for the PCI-Device */
    if (xpcGetPCIDeviceInfo(0x186C, devId, XPC_NO_SUB, XPC_NO_SUB, bus, slot, &pciinfo)) {
            sprintf(msg,"%s (bus %d,slot %d): board not present",devName, bus, slot );
            ssSetErrorStatus(S,msg);
            return;
    }

    /* Map I/O space and mark it read/write */
    Physical1 = (void *)pciinfo.BaseAddress[2];
    base_adc = (volatile uint16_T *)xpcReserveMemoryRegion(Physical1, 128,
        XPC_RT_PG_USERREADWRITE);
    ssSetIWorkValue(S, BASE_ADC_I_IND, (int32_T) base_adc);

    /* Set Digital Output to Initial State */

    base_adc[0x08] = 0;

#endif /* MATLAB_MEX_FILE */

}

static void mdlOutputs(SimStruct *S, int_T tid)
{

#ifndef MATLAB_MEX_FILE

    size_t i;
    InputRealPtrsType uPtrs;
    uint_T mf624DOPortData;
    volatile uint16_T* base_adc;

    base_adc = (volatile uint16_T*) ssGetIWorkValue(S, BASE_ADC_I_IND);

    /* Send Input to Digital Output */
    mf624DOPortData = 0;

    for (i=0;i < mxGetNumberOfElements(CHANNEL_ARG);i++) {
        uPtrs = ssGetInputPortRealSignalPtrs(S,i);
        mf624DOPortData |= ((short)*uPtrs[0] << (((short)mxGetPr(CHANNEL_ARG)[i])-1));
    }

    base_adc[0x08] = mf624DOPortData;

#endif /* METLAB_MEX_FILE */
}

static void mdlTerminate(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE

    volatile uint16_T* base_adc;

    base_adc = (volatile uint16_T*) ssGetIWorkValue(S, BASE_ADC_I_IND);

    /* Set Final State of digital output */

    base_adc[0x08] = 0;

    ssSetUserData(S,NULL);

#endif /* MATLAB_MEX_FILE */
}

/*=============================*
 * Required S-function trailer *
 *=============================*/

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
