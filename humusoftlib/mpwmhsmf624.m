function [Sample, Channels] = mpwmhsmf624(flag, boardType, Channels, Sample)

% MPWMHSMF624 - InitFcn and Mask Initialization for Humusoft MF624 PWM section

% Copyright 1996-2014 Humusoft s.r.o. and The MathWorks, Inc.


% Flag 0 - cross check for duplicate blocks
if flag == 0

  % check channels
  MAX_CHANNEL = 4;
  Channels = evalin('base', get_param(gcb, 'Channels'));
  for i=1:numel(Channels)
    if (Channels(i)<1 || Channels(i)>MAX_CHANNEL)
      error(message('xPCTarget:MF624:ChannelValue', MAX_CHANNEL));
    end
  end

  % get all boards of this type
  ck = mxpccrosscheckers;
  boards = ck.pciuniqa();
  
  % also get boards for Counter Input because they share hardware resources
  if boardType == 1
      boards = [boards; ...
          find_system(bdroot, ...
          'FollowLinks', 'on', ...
          'LookUnderMasks', 'all', ...
          'MaskType', 'ctrhsmf624')];
  elseif boardType == 3
          boards = [boards; ...
          find_system(bdroot, ...
          'FollowLinks', 'on', ...
          'LookUnderMasks', 'all', ...
          'MaskType', 'ctrhsmf634')];
  end
  nboards = numel(boards);
  if nboards<=1
    return;
  end

  % check for duplicate channels
  myslot = evalin('base', get_param(gcb, 'Slot'));
  slots = cell(1, nboards);
  for bd = 1:nboards
    slots{bd} = evalin('base', get_param(boards{bd}, 'Slot'));
  end
  ck.pcichan(boards, myslot, slots, 'Channels', MAX_CHANNEL);
  return;
end


% Flag 1 - check parameters and compose mask display string

% Filter channels to eliminate duplicate - allow only oneinput port per
% channel
Channels = unique(Channels);

% display board type
brdtypes = {'MF624', 'AD622', 'MF634'};
maskDisplay = sprintf('disp(''%s\\nHumusoft\\nPWM Output'');\n', brdtypes{boardType});

% label output ports
maskDisplay = [maskDisplay ...
               sprintf('port_label(''input'', %d, ''%d (f)'');\nport_label(''input'', %d, ''%d (dc)'');\n', ...
                       [2*(1:numel(Channels))-1; Channels; 2*(1:numel(Channels)); Channels])];

% set MaskDisplay string
set_param(gcb, 'MaskDisplay', maskDisplay);

% check sample time
switch numel(Sample)
  case 1
    Sample = [Sample 0];
  case 2
    % OK
  otherwise
    error(message('xPCTarget:MF624:SampleTime'));
end


%% EOF mpwmhsmf624.m
