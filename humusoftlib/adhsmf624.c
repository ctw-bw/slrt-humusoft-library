/* adhsmf624.c - xPC Target, non-inlined S-function driver for A/D section of Humusoft MF624 board  */
/* Copyright 1996-2009 Humusoft s.r.o. and The MathWorks, Inc.
*/

#define 	S_FUNCTION_LEVEL 	2
#define 	S_FUNCTION_NAME 	adhsmf624

#include 	<stddef.h>
#include 	<stdlib.h>

#include 	"simstruc.h"

#ifdef 		MATLAB_MEX_FILE
#include 	"mex.h"
#else
#include    <windows.h>
#include    "xpctarget.h"
#endif


/* Input Arguments */
#define NUM_PARAMS             (5)
#define SLOT_ARG               (ssGetSFcnParam(S,0))
#define DEV_ARG                (ssGetSFcnParam(S,1))
#define CHANNEL_ARG            (ssGetSFcnParam(S,2))
#define RANGE_ARG              (ssGetSFcnParam(S,3))
#define SAMPLE_TIME_PARAM      (ssGetSFcnParam(S,4))

/* Convert S Function Parameters to Variables */

#define SAMPLE_TIME            ((real_T) mxGetPr(SAMPLE_TIME_PARAM)[0])
#define SAMPLE_OFFSET          ((real_T) mxGetPr(SAMPLE_TIME_PARAM)[1])
#define SAMP_TIME_IND          (0)

/* IWork indexes */
enum
{
  BASE_CHIPSET_I_IND,
  BASE_ADC_I_IND,
  BASE_ALTERA_I_IND,
  NO_I_WORKS    /* this must be the last item in enum */
};

#define NO_R_WORKS             (0)

static char_T msg[256];


/*====================*
 * S-function methods *
 *====================*/

static void mdlCheckParameters(SimStruct *S)
{
}

static void mdlInitializeSizes(SimStruct *S)
{
    size_t i;

    ssSetNumSFcnParams(S, NUM_PARAMS);
    if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)) {
        mdlCheckParameters(S);
        if (ssGetErrorStatus(S) != NULL) {
            return;
        }
    } else {
        return; /* Parameter mismatch will be reported by Simulink */
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);



    if (!ssSetNumInputPorts(S, 0)) return;

    if (!ssSetNumOutputPorts(S, (int_T) mxGetNumberOfElements(CHANNEL_ARG))) return;

    for (i=0;i<mxGetNumberOfElements(CHANNEL_ARG);i++) {
        ssSetOutputPortWidth(S, i, 1);
    }

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, 0);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for (i=0; i<NUM_PARAMS; i++)
      ssSetSFcnParamTunable(S, i, SS_PRM_NOT_TUNABLE);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE | SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME);
    ssSetSimStateCompliance(S, HAS_NO_SIM_STATE);
}



static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, SAMPLE_OFFSET);
}



#define MDL_START
static void mdlStart(SimStruct *S)
{

#ifndef MATLAB_MEX_FILE


    size_t i;
    xpcPCIDevice pciinfo;
    void *Physical1, *Physical0, *Physical2;
    volatile uint32_T* base_chipset;
    volatile uint16_T* base_adc;
    volatile uint32_T* base_altera;
    char devName[20];
    uint16_T devId;
    int32_T bus, slot;
    int nbytes; // used to specify the number of byte for BAR0
    int addr_position; // used to indicate index of BAR2

    static volatile unsigned dummy;

    switch  ((int_T)mxGetPr(DEV_ARG)[0]) {
        case 1:
            strcpy(devName,"Humusoft MF624");
            devId=0x0624;
            nbytes = 32;
            addr_position = 4;
            break;
        case 2:
            strcpy(devName,"Humusoft AD622");
            devId=0x0622;
            nbytes = 32;
            addr_position = 4;
            break;
        case 3:
            strcpy(devName,"Humusoft MF634");
            devId=0x0634;
            nbytes = 512;
            addr_position = 3;
            break;
         }

    /* get bus and slot information */
    if (mxGetN(SLOT_ARG) == 1) {
        bus = 0;
        slot = (int32_T)mxGetPr(SLOT_ARG)[0];
    } else {
        bus = (int32_T)mxGetPr(SLOT_ARG)[0];
        slot = (int32_T)mxGetPr(SLOT_ARG)[1];
    }

    /* look for the PCI-Device */
    if (xpcGetPCIDeviceInfo(0x186C, devId, XPC_NO_SUB, XPC_NO_SUB, bus, slot, &pciinfo)) {
            sprintf(msg,"%s (bus %d,slot %d): board not present",devName, bus, slot );
            ssSetErrorStatus(S,msg);
            return;
    }

    /* Map I/O space and mark it read/write */
    Physical0 = (void *)pciinfo.BaseAddress[0];
    base_chipset = (volatile uint32_T *)xpcReserveMemoryRegion(Physical0, nbytes,
        XPC_RT_PG_USERREADWRITE);
    ssSetIWorkValue(S, BASE_CHIPSET_I_IND, (int32_T) base_chipset);

    Physical1 = (void *)pciinfo.BaseAddress[2];
    base_adc = (volatile uint16_T *)xpcReserveMemoryRegion(Physical1, 128,
        XPC_RT_PG_USERREADWRITE);
    ssSetIWorkValue(S, BASE_ADC_I_IND, (int32_T) base_adc);

    Physical2 = (void *)pciinfo.BaseAddress[addr_position];
    base_altera = (volatile uint32_T *)xpcReserveMemoryRegion(Physical2, 128,
    XPC_RT_PG_USERREADWRITE);
    ssSetIWorkValue(S, BASE_ALTERA_I_IND, (int32_T) base_altera);

    /* Check That The Hardware is functioning */

    base_adc[0x00] = 0xff;        // select all channels
    dummy = base_adc[0x10];        // start conversion

    if ((int_T)mxGetPr(DEV_ARG)[0] == 3) {
        if (!(base_altera[0x1A] & 0x020000)) { /* if data ready, error */
        sprintf(msg,"%s (%d): test A/D conversion failed", devName, slot);
        ssSetErrorStatus(S,msg);
        return;
        }
        
        for (i=0;i<0x10000;i++)
            if (!(base_altera[0x1A] & 0x020000)) break;
        if (i==0x10000) {               /* if no conversion, error */
            sprintf(msg,"%s (%d): test A/D conversion failed", devName, slot);
            ssSetErrorStatus(S,msg);
            return;
        }
    } else {
        if (!(base_chipset[0x15] & 0x020000)) { /* if data ready, error */
        sprintf(msg,"%s (%d): test A/D conversion failed", devName, slot);
        ssSetErrorStatus(S,msg);
        return;
        }
        
        for (i=0;i<0x10000;i++)
            if (!(base_chipset[0x15] & 0x020000)) break;
        if (i==0x10000) {               /* if no conversion, error */
            sprintf(msg,"%s (%d): test A/D conversion failed", devName, slot);
            ssSetErrorStatus(S,msg);
            return;
        }
    }

#endif

}

static void mdlOutputs(SimStruct *S, int_T tid)
{

#ifndef MATLAB_MEX_FILE

    size_t i;
    uint_T channel;
    volatile uint32_T* base_chipset;
    volatile uint16_T* base_adc;
    volatile uint32_T* base_adc32;
    volatile uint32_T* base_altera;
    real_T *y;
    static volatile unsigned dummy;
    unsigned int buffer32[4];
    short *buffer16;

    buffer16 = (short*) buffer32;
    base_chipset  = (volatile uint32_T*) ssGetIWorkValue(S, BASE_CHIPSET_I_IND);
    base_adc = (volatile uint16_T*) ssGetIWorkValue(S, BASE_ADC_I_IND);
    base_adc32 = (volatile uint32_T*) base_adc;
    base_altera = (volatile uint32_T*) ssGetIWorkValue(S, BASE_ALTERA_I_IND);
    dummy = base_adc[0x10];                    // start conversion

    if ((int_T)mxGetPr(DEV_ARG)[0] == 3) {
        while (base_altera[0x1A] == 0) {  // data ready
        }
    } else {
        while (base_chipset[0x15] & 0x020000 == 0) {  // data ready
        }
    }

    buffer32[0] = base_adc32[0];                   // read data
    buffer32[1] = base_adc32[0];
    buffer32[2] = base_adc32[0];
    buffer32[3] = base_adc32[0];
    
    for (i = 0; i < mxGetNumberOfElements(CHANNEL_ARG); i++) {
        channel = (uint_T)mxGetPr(CHANNEL_ARG)[i] - 1;
        y = ssGetOutputPortSignal(S, i);
        
        y[0] = ((short)(buffer16[channel] << 2)) * 10.0 / 32768.0;
    }

#endif

}

static void mdlTerminate(SimStruct *S)
{
}


#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif


