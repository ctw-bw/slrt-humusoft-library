function [Sample, Channels] = menchsmf624(flag, boardType, Channels, Filter, Index, Sample)

% MENCHSMF624 - InitFcn and Mask Initialization for Humusoft MF624 encoder section

% Copyright 1996-2014 Humusoft s.r.o. and The MathWorks, Inc.


% Flag 0 - cross check for duplicate blocks
if flag == 0

  % check channels
  MAX_CHANNEL = 4;
  Channels = evalin('base', get_param(gcb, 'Channels'));
  for i=1:numel(Channels)
    if (Channels(i)<1 || Channels(i)>MAX_CHANNEL)
      error(message('xPCTarget:MF624enc:ChannelValue', MAX_CHANNEL));
    end
  end

  % get all boards of this type
  ck = mxpccrosscheckers;
  boards = ck.pciuniqa();
  nboards = numel(boards);
  if nboards<=1
    return;
  end

  % check for duplicate channels
  myslot = evalin('base', get_param(gcb, 'Slot'));
  slots = cell(1, nboards);
  for bd = 1:nboards
    slots{bd} = evalin('base', get_param(boards{bd}, 'Slot'));
  end
  ck.pcichan(boards, myslot, slots, 'Channels', MAX_CHANNEL);
  return;
end


% Flag 1 - check parameters and compose mask display string

% Filter channels to eliminate duplicate - allow only one output port per
% channel
Channels = unique(Channels);

% display board type
brdtypes = {'MF624', 'AD622', 'MF634'};
maskDisplay = sprintf('disp(''%s\\nHumusoft\\nEncoder Input'');\n', brdtypes{boardType});

% label output ports
maskDisplay = [maskDisplay ...
               sprintf('port_label(''output'', %d, ''%d'');\n', ...
                       [1:numel(Channels); Channels])];

% set MaskDisplay string
set_param(gcb, 'MaskDisplay', maskDisplay);

% check parameters
chnLength = numel(Channels);
filterLength = numel(Filter);
if filterLength ~= chnLength
  error (message('xPCTarget:MF624enc:FilterDim'));
end

for i = 1:filterLength
  if (Filter(i) ~=0 && Filter(i) ~=1)
    error (message('xPCTarget:MF624enc:FilterValue'));
  end
end

indexLength = numel(Index);
if indexLength ~= chnLength
  error (message('xPCTarget:MF624enc:IndexDim'));
end

% check sample time
switch numel(Sample)
  case 1
    Sample = [Sample 0];
  case 2
    % OK
  otherwise
    error(message('xPCTarget:MF624enc:SampleTime'));
end


%% EOF menchsmf624.m
