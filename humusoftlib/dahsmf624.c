/* dahsmf624.c - xPC Target, non-inlined S-function driver for Humusoft MF624 D/A section */
/* Copyright 1996-2009 Humusoft s.r.o. and The MathWorks, Inc.
*/

#define S_FUNCTION_LEVEL 2
#define S_FUNCTION_NAME  dahsmf624

#include 	<stddef.h>
#include 	<stdlib.h>
#include 	<math.h>

#include 	"simstruc.h"

#ifdef 		MATLAB_MEX_FILE
#include 	"mex.h"
#else
#include 	<windows.h>
#include        "xpctarget.h"
#endif

/* Input Arguments */
#define NUM_PARAMS             (5)
#define SLOT_ARG               (ssGetSFcnParam(S,0))
#define DEV_ARG                (ssGetSFcnParam(S,1))
#define CHANNEL_ARG            (ssGetSFcnParam(S,2))
#define RANGE_ARG              (ssGetSFcnParam(S,3))
#define SAMPLE_TIME_PARAM      (ssGetSFcnParam(S,4))

/* Convert S Function Parameters to Varibles */

#define SAMPLE_TIME            ((real_T) mxGetPr(SAMPLE_TIME_PARAM)[0])
#define SAMPLE_OFFSET          ((real_T) mxGetPr(SAMPLE_TIME_PARAM)[1])
#define SAMP_TIME_IND          (0)

/* IWork indexes */
enum
{
  BASE_CHIPSET_I_IND,
  BASE_ADC_I_IND,
  BASE_ALTERA_I_IND,
  NO_I_WORKS    /* this must be the last item in enum */
};

#define NO_R_WORKS             (0)

static char_T msg[256];


#ifndef MATLAB_MEX_FILE
static void DA_Output(volatile uint16_T* base_adc, uint_T channel, real_T value)
{
    real_T out;

    out=8192*(1-value/-10);

    out=max(out,0);
    out=min(out,16383);
    out=floor(out+0.5);

    base_adc[0x10+channel] =(uint_T)out;
}
#endif

/*====================*
 * S-function methods *
 *====================*/

static void mdlCheckParameters(SimStruct *S)
{
}

static void mdlInitializeSizes(SimStruct *S)
{
    size_t i;

    ssSetNumSFcnParams(S, NUM_PARAMS);
    if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)) {
        mdlCheckParameters(S);
        if (ssGetErrorStatus(S) != NULL) {
            return;
        }
    } else {
        return; /* Parameter mismatch will be reported by Simulink */
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, (int_T) mxGetNumberOfElements(CHANNEL_ARG))) return;

    for (i=0;i<mxGetNumberOfElements(CHANNEL_ARG);i++) {
        ssSetInputPortWidth(S, i, 1);
    }

    if (!ssSetNumOutputPorts(S, 0)) return;

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, 0);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for (i=0; i<NUM_PARAMS; i++)
      ssSetSFcnParamTunable(S, i, SS_PRM_NOT_TUNABLE);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE | SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME);
    ssSetSimStateCompliance(S, HAS_NO_SIM_STATE);
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, SAMPLE_OFFSET);
}


#define MDL_START
static void mdlStart(SimStruct *S)
{

#ifndef MATLAB_MEX_FILE
    size_t i;
    xpcPCIDevice pciinfo;
    void *Physical1, *Physical0, *Physical2;
    volatile uint32_T* base_chipset;
    volatile uint16_T* base_adc;
    volatile uint32_T* base_altera;
    char devName[20];
    uint16_T devId;
    int32_T bus, slot;
    uint32_T nbytes, addr_position;


    switch ((int_T)mxGetPr(DEV_ARG)[0]) {
        case 1:
            strcpy(devName,"Humusoft MF624");
            devId=0x0624;
            nbytes = 32;
            addr_position = 4;
            break;
        case 2:
            strcpy(devName,"Humusoft AD622");
            devId=0x0622;
            nbytes = 32;
            addr_position = 4;
            break;
        case 3:
            strcpy(devName,"Humusoft MF634");
            devId=0x0634;
            nbytes = 512;
            addr_position = 3;
            break;
         }

    /* get bus and slot information */
    if (mxGetN(SLOT_ARG) == 1) {
        bus = 0;
        slot = (int32_T)mxGetPr(SLOT_ARG)[0];
    } else {
        bus = (int32_T)mxGetPr(SLOT_ARG)[0];
        slot = (int32_T)mxGetPr(SLOT_ARG)[1];
    }

    /* look for the PCI-Device */
    if (xpcGetPCIDeviceInfo(0x186C, devId, XPC_NO_SUB, XPC_NO_SUB, bus, slot, &pciinfo)) {
            sprintf(msg,"%s (bus %d,slot %d): board not present",devName, bus, slot );
            ssSetErrorStatus(S,msg);
            return;
    }

    /* Map I/O space and mark it read/write */   
    
    Physical0 = (void *)pciinfo.BaseAddress[0];
    base_chipset = (volatile uint32_T *)xpcReserveMemoryRegion(Physical0, nbytes,
        XPC_RT_PG_USERREADWRITE);
    ssSetIWorkValue(S, BASE_CHIPSET_I_IND, (int32_T) base_chipset);

    Physical1 = (void *)pciinfo.BaseAddress[2];
    base_adc = (volatile uint16_T *)xpcReserveMemoryRegion(Physical1, 128,
        XPC_RT_PG_USERREADWRITE);
    ssSetIWorkValue(S, BASE_ADC_I_IND, (int32_T) base_adc);

    Physical2 = (void *)pciinfo.BaseAddress[addr_position];
    base_altera = (volatile uint32_T *)xpcReserveMemoryRegion(Physical2, 128,
    XPC_RT_PG_USERREADWRITE);
    ssSetIWorkValue(S, BASE_ALTERA_I_IND, (int32_T) base_altera);


/* Set initial outputs to zero */
    for (i=0;i<mxGetNumberOfElements(CHANNEL_ARG);i++) {
        DA_Output(base_adc, (uint_T)mxGetPr(CHANNEL_ARG)[i]-1, 0);
    }
    if ((int_T)mxGetPr(DEV_ARG)[0] == 3) {
        base_altera[0x1A] = 0x04000002;
    } else {
        base_chipset[0x15] = 0x064006C0;  
    }

  
#endif /* MATLAB_MEX_FILE */

}

static void mdlOutputs(SimStruct *S, int_T tid)
{
#ifndef MATLAB_MEX_FILE

    InputRealPtrsType uPtrs;
    size_t i;
    volatile uint32_T* base_chipset;
    volatile uint16_T* base_adc;
    volatile uint32_T* base_altera;

    base_chipset  = (volatile uint32_T*) ssGetIWorkValue(S, BASE_CHIPSET_I_IND);
    base_altera = (volatile uint32_T*) ssGetIWorkValue(S, BASE_ALTERA_I_IND);

    if ((int_T)mxGetPr(DEV_ARG)[0] == 3) {
        base_altera[0x1A] = base_altera[0x1A]|0x00800000;
    } else {
        base_chipset[0x15] = 0x068006C0;        // disable update
    }

    
    base_adc = (volatile uint16_T*) ssGetIWorkValue(S, BASE_ADC_I_IND);
    for (i=0;i<mxGetNumberOfElements(CHANNEL_ARG);i++) {
        uPtrs = ssGetInputPortRealSignalPtrs(S,i);
        DA_Output(base_adc, (uint_T)mxGetPr(CHANNEL_ARG)[i]-1, *uPtrs[0]);
    }

    if ((int_T)mxGetPr(DEV_ARG)[0] == 3) {
        base_altera[0x1A] = 0x04000002;
    } else {
        base_chipset[0x15] = 0x064006C0;       // simultaneous update
    }

    
#endif /* MATLAB_MEX_FILE */

}


static void mdlTerminate(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE
    size_t i;
    volatile uint32_T* base_chipset;
    volatile uint16_T* base_adc;
    volatile uint32_T* base_altera;

    base_chipset  = (volatile uint32_T*) ssGetIWorkValue(S, BASE_CHIPSET_I_IND);
    base_altera = (volatile uint32_T*) ssGetIWorkValue(S, BASE_ALTERA_I_IND);     


    if ((int_T)mxGetPr(DEV_ARG)[0] == 3) {
        base_altera[0x1A] = 0x04800002;
    } else {
        base_chipset[0x15] = 0x068006C0;        // disable update
    }

/* Set final outputs to zero */
    base_adc = (volatile uint16_T*) ssGetIWorkValue(S, BASE_ADC_I_IND);
    for (i=0;i < mxGetNumberOfElements(CHANNEL_ARG);i++) {
        DA_Output(base_adc, (uint_T)mxGetPr(CHANNEL_ARG)[i]-1, 0);
    }

    if ((int_T)mxGetPr(DEV_ARG)[0] == 3) {
        base_altera[0x1A] = 0x04000002;
    } else {
        base_chipset[0x15] = 0x064006C0;       // simultaneous update
    }

#endif /* MATLAB_MEX_FILE */
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
