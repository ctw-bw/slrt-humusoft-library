function [Sample, Channels] = mdihsmf624(flag, boardType, Channels, Sample)

% MDIHSMF624 - InitFcn and Mask Initialization for Humusoft MF624 digital input section

% Copyright 1996-2014 Humusoft s.r.o. and The MathWorks, Inc.


% Flag 0 - cross check for duplicate blocks
if flag == 0
  % no cross-check necessary
  return;
end


% Flag 1 - check parameters and compose mask display string

% Filter channels to eliminate duplicate - allow only one output port per
% channel
Channels = unique(Channels);

% display board type
brdtypes = {'MF624', 'AD622', 'MF634'};
maskDisplay = sprintf('disp(''%s\\nHumusoft\\nDigital Input'');\n', brdtypes{boardType});

% label output ports
maskDisplay = [maskDisplay ...
               sprintf('port_label(''output'', %d, ''%d'');\n', ...
                       [1:numel(Channels); Channels])];

% set MaskDisplay string
set_param(gcb, 'MaskDisplay', maskDisplay);

% check channels
MAX_CHANNEL = 8;
for i=1:numel(Channels)
  if (Channels(i)<1 || Channels(i)>MAX_CHANNEL)
    error(message('xPCTarget:MF624DI:ChannelValue', MAX_CHANNEL));
   end
end

% check sample time
switch numel(Sample)
  case 1
    Sample = [Sample 0];
  case 2
    % OK
  otherwise
    error(message('xPCTarget:MF624DI:SampleTime'));
end


% EOF mdihsmf624.m
