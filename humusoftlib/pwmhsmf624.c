/* pwmhsmf624.c - xPC Target, non-inlined S-function driver for Humusoft MF624 PWM section */
/* Copyright 1996-2009 Humusoft s.r.o. and The MathWorks, Inc.
*/

#define S_FUNCTION_LEVEL 2
#define S_FUNCTION_NAME  pwmhsmf624

#include 	<stddef.h>
#include 	<stdlib.h>
#include 	<math.h>

#include 	"simstruc.h"

#ifdef 		MATLAB_MEX_FILE
#include 	"mex.h"
#else
#include 	<windows.h>
#include        "xpctarget.h"
#endif

/* Input Arguments */
#define NUM_PARAMS             (4)
#define SLOT_ARG               (ssGetSFcnParam(S,0))
#define DEV_ARG                (ssGetSFcnParam(S,1))
#define CHANNEL_ARG            (ssGetSFcnParam(S,2))
#define SAMPLE_TIME_PARAM      (ssGetSFcnParam(S,3))

/* Convert S Function Parameters to Varibles */

#define DEVICE                 ((uint_T) mxGetPr(DEV_ARG)[0])
#define SAMPLE_TIME            ((real_T) mxGetPr(SAMPLE_TIME_PARAM)[0])
#define SAMPLE_OFFSET          ((real_T) mxGetPr(SAMPLE_TIME_PARAM)[1])
#define SAMP_TIME_IND          (0)

/* IWork indexes */
enum
{
  BASE_ALTERA_I_IND,
  NO_I_WORKS    /* this must be the last item in enum */
};

#define NO_R_WORKS             (0)

static char_T msg[256];



/*====================*
 * S-function methods *
 *====================*/

static void mdlCheckParameters(SimStruct *S)
{
}

static void mdlInitializeSizes(SimStruct *S)
{
    size_t i;

    ssSetNumSFcnParams(S, NUM_PARAMS);
    if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)) {
        mdlCheckParameters(S);
        if (ssGetErrorStatus(S) != NULL) {
            return;
        }
    } else {
        return; /* Parameter mismatch will be reported by Simulink */
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);

    if (!ssSetNumInputPorts(S, 2*((int_T) mxGetNumberOfElements(CHANNEL_ARG)))) return;

    for (i=0;i<2*mxGetNumberOfElements(CHANNEL_ARG);i++) {
        ssSetInputPortWidth(S, i, 1);
    }

    if (!ssSetNumOutputPorts(S, 0)) return;

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, 0);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for (i=0; i<NUM_PARAMS; i++)
      ssSetSFcnParamTunable(S, i, SS_PRM_NOT_TUNABLE);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE | SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME);
    ssSetSimStateCompliance(S, HAS_NO_SIM_STATE);
}

static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, SAMPLE_OFFSET);
}


#define MDL_START
static void mdlStart(SimStruct *S)
{

#ifndef MATLAB_MEX_FILE
    size_t i;
    uint_T channel;
    xpcPCIDevice pciinfo;
    void *Physical2;
    volatile uint32_T* base_altera;
    char devName[20];
    uint16_T devId;
    int32_T bus, slot, addr_position;


    switch ((int_T)mxGetPr(DEV_ARG)[0]) {
        case 1:
            strcpy(devName,"Humusoft MF624");
            devId=0x0624;
            addr_position = 4;
            break;
        case 3:
            strcpy(devName,"Humusoft MF634");
            devId=0x0634;
            addr_position = 3;
            break;
         }

    /* get bus and slot information */
    if (mxGetN(SLOT_ARG) == 1) {
        bus = 0;
        slot = (int32_T)mxGetPr(SLOT_ARG)[0];
    } else {
        bus = (int32_T)mxGetPr(SLOT_ARG)[0];
        slot = (int32_T)mxGetPr(SLOT_ARG)[1];
    }

    /* look for the PCI-Device */
    if (xpcGetPCIDeviceInfo(0x186C, devId, XPC_NO_SUB, XPC_NO_SUB, bus, slot, &pciinfo)) {
            sprintf(msg,"%s (bus %d,slot %d): board not present",devName, bus, slot );
            ssSetErrorStatus(S,msg);
            return;
    }

    /* Map I/O space and mark it read/write */
   
    Physical2 = (void *)pciinfo.BaseAddress[addr_position];
    base_altera = (volatile uint32_T *)xpcReserveMemoryRegion(Physical2, 128,
        XPC_RT_PG_USERREADWRITE);
    ssSetIWorkValue(S, BASE_ALTERA_I_IND, (int32_T) base_altera);

/* Set initial conditions */
    for (i=0;i<mxGetNumberOfElements(CHANNEL_ARG);i++) {
      channel=(uint_T)mxGetPr(CHANNEL_ARG)[i]-1;

      base_altera[0x18] = 0x02 << (6*channel);        // stop counter
	  base_altera[0x00+0x04*channel] = 0x2E;          // set counter mode
	  base_altera[0x18] = 0x28 << (6*channel);        // reset counter and reset toggle output
	  base_altera[0x01+0x04*channel] = 0;             // reset Load A register
	  base_altera[0x02+0x04*channel] = 0;             // reset Load B register
	  base_altera[0x18] = 0x01 << (6*channel);        // start counter
    }

#endif /* MATLAB_MEX_FILE */

}

static void mdlOutputs(SimStruct *S, int_T tid)
{
#ifndef MATLAB_MEX_FILE

    InputRealPtrsType uPtrs_freq, uPtrs_duty;
    size_t i;
    uint_T channel;
    volatile uint32_T* base_altera;
    double freq, duty;
    double T;
    uint32_T T1, T2, mode;

    base_altera  = (volatile uint32_T*) ssGetIWorkValue(S, BASE_ALTERA_I_IND);

    for (i=0;i<mxGetNumberOfElements(CHANNEL_ARG);i++) {
        channel=(uint_T)mxGetPr(CHANNEL_ARG)[i]-1;
        uPtrs_freq = ssGetInputPortRealSignalPtrs(S,2*i);
        uPtrs_duty = ssGetInputPortRealSignalPtrs(S,2*i+1);

        freq = *uPtrs_freq[0];
        duty = *uPtrs_duty[0];

        freq = max(freq, 0.012);              // limit inputs
        duty = max(duty, 0);
        duty = min(duty, 1);

        T=50000000/freq;
        T=floor(T + 0.5);
        T1=(uint32_T) max(1,floor(T*duty + 0.5));
        T2=(uint32_T) max(1,floor(T)-T1);


        base_altera[0x01+0x04*channel] = T1-1;
        base_altera[0x02+0x04*channel] = T2-1;
        mode=0x0E;
        if (duty == 0) mode=0x2E;            // force output low
        if (duty == 1) mode=0x3E;            // force output high
        base_altera[0x04*channel] = mode;
    }

#endif /* MATLAB_MEX_FILE */

}


static void mdlTerminate(SimStruct *S)
{
#ifndef MATLAB_MEX_FILE
    size_t i;
    uint_T channel;
    volatile uint32_T* base_altera;

    base_altera  = (volatile uint32_T*) ssGetIWorkValue(S, BASE_ALTERA_I_IND);

/* Set final outputs to zero */
    for (i=0;i < mxGetNumberOfElements(CHANNEL_ARG);i++) {
        channel = (uint_T)mxGetPr(CHANNEL_ARG)[i]-1;
        base_altera[0x04*channel] = 0x2E;             // force output low
    }

#endif /* MATLAB_MEX_FILE */
}

#ifdef  MATLAB_MEX_FILE    /* Is this file being compiled as a MEX-file? */
#include "simulink.c"      /* MEX-file interface mechanism */
#else
#include "cg_sfun.h"       /* Code generation registration function */
#endif
