/* enchsmf624.c - xPC Target, non-inlined S-function driver for encoder section of Humusoft MF624 board  */
/* Copyright 1996-2010 Humusoft s.r.o. and The MathWorks, Inc.
*/

#define 	S_FUNCTION_LEVEL 	2
#define 	S_FUNCTION_NAME 	enchsmf624

#include 	<stddef.h>
#include 	<stdlib.h>

#include 	"simstruc.h"

#ifdef 		MATLAB_MEX_FILE
#include 	"mex.h"
#else
#include 	<windows.h>
#include        "xpctarget.h"
#endif


/* Input Arguments */
#define NUM_PARAMS             (6)
#define SLOT_ARG               (ssGetSFcnParam(S,0))
#define DEV_ARG                (ssGetSFcnParam(S,1))
#define CHANNEL_ARG            (ssGetSFcnParam(S,2))
#define FILTER_ARG             (ssGetSFcnParam(S,3))
#define INDEX_ARG              (ssGetSFcnParam(S,4))
#define SAMPLE_TIME_PARAM      (ssGetSFcnParam(S,5))

/* Convert S Function Parameters to Varibles */

#define SAMPLE_TIME            ((real_T) mxGetPr(SAMPLE_TIME_PARAM)[0])
#define SAMPLE_OFFSET          ((real_T) mxGetPr(SAMPLE_TIME_PARAM)[1])
#define SAMP_TIME_IND          (0)

/* IWork indexes */
enum
{
  BASE_ALTERA_I_IND,
  NO_I_WORKS    /* this must be the last item in enum */
};

#define NO_R_WORKS             (0)

#define MAXIRCFILT 2500000

static char_T msg[256];


/*====================*
 * S-function methods *
 *====================*/

static void mdlCheckParameters(SimStruct *S)
{
}

static void mdlInitializeSizes(SimStruct *S)
{
    size_t i;

    ssSetNumSFcnParams(S, NUM_PARAMS);
    if (ssGetNumSFcnParams(S) == ssGetSFcnParamsCount(S)) {
        mdlCheckParameters(S);
        if (ssGetErrorStatus(S) != NULL) {
            return;
        }
    } else {
        return; /* Parameter mismatch will be reported by Simulink */
    }

    ssSetNumContStates(S, 0);
    ssSetNumDiscStates(S, 0);



    if (!ssSetNumInputPorts(S, 0)) return;

    if (!ssSetNumOutputPorts(S, (int_T) mxGetNumberOfElements(CHANNEL_ARG))) return;

    for (i=0;i<mxGetNumberOfElements(CHANNEL_ARG);i++) {
        ssSetOutputPortWidth(S, i, 1);
    }

    ssSetNumSampleTimes(S, 1);
    ssSetNumRWork(S, NO_R_WORKS);
    ssSetNumIWork(S, NO_I_WORKS);
    ssSetNumPWork(S, 0);

    ssSetNumModes(S, 0);
    ssSetNumNonsampledZCs(S, 0);

    for (i=0; i<NUM_PARAMS; i++)
      ssSetSFcnParamTunable(S, i, SS_PRM_NOT_TUNABLE);

    ssSetOptions(S, SS_OPTION_EXCEPTION_FREE_CODE | SS_OPTION_DISALLOW_CONSTANT_SAMPLE_TIME);
    ssSetSimStateCompliance(S, HAS_NO_SIM_STATE);
}



static void mdlInitializeSampleTimes(SimStruct *S)
{
    ssSetSampleTime(S, 0, SAMPLE_TIME);
    ssSetOffsetTime(S, 0, SAMPLE_OFFSET);
}



#define MDL_START
static void mdlStart(SimStruct *S)
{

#ifndef MATLAB_MEX_FILE

    xpcPCIDevice pciinfo;
    void *Physical2;
    volatile uint32_T* base_altera;
    char devName[20];
    uint16_T devId;
    int32_T bus, slot, addr_position;

    uint32_T irc_shadow;
    uint_T channel;
    unsigned mode;
    size_t i;

    switch ((int_T)mxGetPr(DEV_ARG)[0]) {
        case 1:
            strcpy(devName,"Humusoft MF624");
            devId=0x0624;
            break;
        case 3:
            strcpy(devName,"Humusoft MF634");
            devId=0x0634;
            break;
         }

    /* get bus and slot information */
    if (mxGetN(SLOT_ARG) == 1) {
        bus = 0;
        slot = (int32_T)mxGetPr(SLOT_ARG)[0];
    } else {
        bus = (int32_T)mxGetPr(SLOT_ARG)[0];
        slot = (int32_T)mxGetPr(SLOT_ARG)[1];
    }

    /* look for the PCI-Device */
    if (xpcGetPCIDeviceInfo(0x186C, devId, XPC_NO_SUB, XPC_NO_SUB, bus, slot, &pciinfo)) {
            sprintf(msg,"%s (bus %d,slot %d): board not present",devName, bus, slot );
            ssSetErrorStatus(S,msg);
            return;
    }

    /* Map I/O space and mark it read/write */
    if ((int_T)mxGetPr(DEV_ARG)[0] == 3) {
        addr_position = 3;
    } else {
        addr_position = 4;
    }

    Physical2 = (void *)pciinfo.BaseAddress[addr_position];
    base_altera = (volatile uint32_T *)xpcReserveMemoryRegion(Physical2, 128,
        XPC_RT_PG_USERREADWRITE);
    ssSetIWorkValue(S, BASE_ALTERA_I_IND, (int32_T) base_altera);

    base_altera[0x1B] = 0x10101010;                                  // set x4 mode, reset counters
    base_altera[0x1B] = 0x0;                                         // set x4 mode, enable counters
    irc_shadow = 0;

    for (i=0;i<mxGetNumberOfElements(CHANNEL_ARG);i++) {
        channel=(uint_T)mxGetPr(CHANNEL_ARG)[i]-1;

        mode = 0;
        switch ((uint_T)mxGetPr(INDEX_ARG)[i]) {
                        case 0:    mode = 0x00; break;	  // Index disabled
			case 1:    mode = 0x20; break;	  // Reset if Index = 0
			case 2:    mode = 0x30; break;    // Reset if Index = 1
			case 3:    mode = 0x40; break;    // Reset on rising edge of Index
			case 4:    mode = 0x50; break;    // Reset on falling edge of Index
			case 5:    mode = 0x60; break;    // Reset on either edge of Index
			case 6:    mode = 0x0C; break;    // Count enabled if Index = 1
			case 7:    mode = 0x08; break;    // Count enabled if Index = 0
		}
        mode |= (mxGetPr(FILTER_ARG)[i] && 0x01) << 7;
        irc_shadow |= mode << (8 * channel);
     }
    base_altera[0x1B] = irc_shadow;

#endif

}

static void mdlOutputs(SimStruct *S, int_T tid)
{

#ifndef MATLAB_MEX_FILE

    size_t i;
    uint_T channel;
    volatile int32_T* base_altera;
    real_T *y;

    base_altera = (volatile int32_T*) ssGetIWorkValue(S, BASE_ALTERA_I_IND);

    for (i=0;i<mxGetNumberOfElements(CHANNEL_ARG);i++) {
        channel=(uint_T)mxGetPr(CHANNEL_ARG)[i]-1;
        y=ssGetOutputPortSignal(S,i);

        y[0]=(real_T) base_altera[0x1C+channel];
    }

#endif

}

static void mdlTerminate(SimStruct *S)
{
}


#ifdef MATLAB_MEX_FILE  /* Is this file being compiled as a MEX-file? */
#include "simulink.c"   /* Mex glue */
#else
#include "cg_sfun.h"    /* Code generation glue */
#endif


